package in.javahelp.payment;

public class EmployeePayment {
	
	String employeeId;
	Double salary;
	String employeeName;
	String pfAccountNo;
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getPfAccountNo() {
		return pfAccountNo;
	}
	public void setPfAccountNo(String pfAccountNo) {
		this.pfAccountNo = pfAccountNo;
	}
	public EmployeePayment(String employeeId, Double salary, String employeeName, String pfAccountNo) {
		super();
		this.employeeId = employeeId;
		this.salary = salary;
		this.employeeName = employeeName;
		this.pfAccountNo = pfAccountNo;
	}
	@Override
	public String toString() {
		return "EmployeePayment [employeeId=" + employeeId + ", salary=" + salary + ", employeeName=" + employeeName
				+ ", pfAccountNo=" + pfAccountNo + "]";
	}
	
	public String getEmployeeDetails(){
		System.out.println("This method has no implementaion");
		
		return null;
	}
	
	
	
	

}
