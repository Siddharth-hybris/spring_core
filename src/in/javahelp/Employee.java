package in.javahelp;

import java.util.Date;

public class Employee {

	String name="hello";
	String ecode="xyz-10";
	
	private Date joiningDate;
	private Date firsrhike;
	
	Address address= new Address();
	public void m1(){
		address.m1();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEcode() {
		return ecode;
	}
	public void setEcode(String ecode) {
		this.ecode = ecode;
	}
	public Date getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}
	public Date getFirsrhike() {
		return firsrhike;
	}
	public void setFirsrhike(Date firsrhike) {
		this.firsrhike = firsrhike;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", ecode=" + ecode + ", joiningDate=" + joiningDate + ", firsrhike="
				+ firsrhike + ", address=" + address + "]";
	}
	public Employee(String name, String ecode, Date joiningDate, Date firsrhike, Address address) {
		super();
		this.name = name;
		this.ecode = ecode;
		this.joiningDate = joiningDate;
		this.firsrhike = firsrhike;
		this.address = address;
	}
	
	
}
